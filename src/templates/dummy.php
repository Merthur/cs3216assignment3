<?php
	$data = array();
	$thisMonth = array();

	$percentage = array(100, 2, 3, 4, 0);
	$seriesName = array("Diet", "Transport", "Recreation", "Households", "Shopping");



	for ($i=0; $i <= 6 ; $i++) { 
		$thisWeek[$i] = ($i + $i) * 1.375;
	}



	$data["thisweek"] = $thisWeek;
	$data["percentage"] = $percentage;
	$data["seriesName"] = $seriesName;
	
	print json_encode($data);
?>